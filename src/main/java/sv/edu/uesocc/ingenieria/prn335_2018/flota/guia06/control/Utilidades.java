/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.guia06.control;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Stateless
@LocalBean
public class Utilidades implements Serializable{
    public EntityManagerFactory emf=Persistence.createEntityManagerFactory("flota_unit");
    public EntityManager em = emf.createEntityManager();
    
    /**
     * Creacion del registro en la base de Datos
     * @param entity registro a crear en la base de Datos
     */
    
     public void insert(TipoEstadoReserva entity) {

        try {
            if (entity != null) {
                em.getTransaction().begin();
                em.persist(entity);
               em.getTransaction().commit();
                System.out.println("registro insertado");
            }else{
                throw new NullPointerException("entity manager nulo");
            }

        } catch (NullPointerException e) {
          em.getTransaction().rollback();
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Error no se guardo !!");

        }

       
    }

    /**
     *
     * @param entity entidad para ser actualizada en la base de datos
     */
    public void update(TipoEstadoReserva entity) {

        try {
            if (entity != null) {
                em.getTransaction().begin();
                em.merge(entity);
                em.getTransaction().commit();
            }
            else{
                throw new NullPointerException("entity manager nulo");
            }

        } catch (NullPointerException e) {
         
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e.getMessage());

        }
     
    }

    /**
     *
     * @param entity entidad para ser eliminada en la base de datos
     */

    public void delete(TipoEstadoReserva entity) {
        if (entity != null) {
     
            try {
                em.getTransaction().begin();
                if (!em.contains(entity)) {
                    entity = em.merge(entity);
                }
                em.remove(entity);
                em.getTransaction().commit();
              
              
            } catch (Exception ex) {
                em.getTransaction().rollback();
                System.out.print(ex);
            }
        }else{
                throw new NullPointerException("entity manager nulo");
            }
       

    }
    
    /**
     *
     * @return todos los registros de mi base de datos
     */
    public List selectAll() {

        List<TipoEstadoReserva> lista = new ArrayList<>();

        try {
            TypedQuery<TipoEstadoReserva> query = em.createQuery("SELECT ter FROM TipoEstadoReserva ter", TipoEstadoReserva.class);

            lista = query.getResultList();

        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e.getMessage());
        }

        return lista;

    }

    /**
     * 
     * @param id identificador del registro a buscar
     * @return una lista con un solo registro 
     */
    public List<TipoEstadoReserva> findByID(int id) {
     
        Query query = em.createQuery("SELECT t FROM TipoEstadoReserva t WHERE t.idTipoEstadoReserva = :idTipoEstadoReserva");
        query.setParameter("idTipoEstadoReserva", id);
        List<TipoEstadoReserva> tpe = query.getResultList();
        return tpe;
    }
    
    
    /**
     * 
     * @param inicio primer registro del rango requerido
     * @param totalRegistros ultimo registro dentro del rango requerido
     * @return una lista con el rango especificado
     */
   public List<TipoEstadoReserva> findRange(int inicio, int totalRegistros){
      
        List<TipoEstadoReserva> ls= new ArrayList<>();
        
        if(totalRegistros>0){
           try {
            TypedQuery<TipoEstadoReserva> q = em.createQuery("SELECT tpe FROM TipoEstadoReserva tpe WHERE tpe.idTipoEstadoReserva>=:ini", TipoEstadoReserva.class);
           q.setParameter("ini", inicio);

             List<TipoEstadoReserva>  list = q.getResultList();
         int i=0;
            for (TipoEstadoReserva ter : list) {
                if(i<totalRegistros){
                    ls.add(ter);
                    i++;
                }else{
                    
                }
                    }
            
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);    
        }
        return ls; 
        }else{
            throw new IllegalArgumentException("parametro debe de ser mayor a cero");
        }
        
        
}
      
}
