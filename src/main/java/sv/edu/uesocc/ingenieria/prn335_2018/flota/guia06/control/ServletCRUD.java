/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.guia06.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@WebServlet(name = "ServletCRUD", urlPatterns = {"/ServletCRUD"})
public class ServletCRUD extends HttpServlet {

    @Inject
    Utilidades util;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            if ("Crear".equals(request.getParameter("insert"))) {
                //Integer id = Integer.parseInt(request.getParameter("id_"));
                String nombre = request.getParameter("name_");
                Boolean activo = Boolean.parseBoolean(request.getParameter("activo"));
                Boolean validacion = Boolean.parseBoolean(request.getParameter("aprobacion"));
                String observacion = request.getParameter("observacion");

                TipoEstadoReserva tpe = new TipoEstadoReserva();

                //tpe.setIdTipoEstadoReserva(id);
                tpe.setNombre(nombre);
                tpe.setActivo(activo);
                tpe.setIndicaAprobacion(validacion);
                tpe.setObservaciones(observacion);

                try {
                    util.insert(tpe);

                } catch (Exception e) {
                }

            } else if ("Actualizar".equals(request.getParameter("actualizar"))) {
                Integer id = Integer.parseInt(request.getParameter("id_"));
                String nombre = request.getParameter("name_");
                Boolean activo = Boolean.valueOf(request.getParameter("activo"));
                Boolean validacion = Boolean.valueOf(request.getParameter("aprobacion"));
                String observacion = request.getParameter("observacion");

                TipoEstadoReserva tpe = new TipoEstadoReserva();

                tpe.setIdTipoEstadoReserva(id);
                tpe.setNombre(nombre);
                tpe.setActivo(activo);
                tpe.setIndicaAprobacion(validacion);
                tpe.setObservaciones(observacion);
                util.update(tpe);
            } else if ("Eliminar".equals(request.getParameter("delete"))) {
                Integer id = Integer.parseInt(request.getParameter("id_"));
                String nombre = request.getParameter("name_");
                Boolean activo = Boolean.valueOf(request.getParameter("activo"));
                Boolean validacion = Boolean.valueOf(request.getParameter("aprobacion"));
                String observacion = request.getParameter("observacion");

                TipoEstadoReserva tpe = new TipoEstadoReserva();

                tpe.setIdTipoEstadoReserva(id);
                tpe.setNombre(nombre);
                tpe.setActivo(activo);
                tpe.setIndicaAprobacion(validacion);
                tpe.setObservaciones(observacion);

                util.delete(tpe);

            } else if ("Buscar".equals(request.getParameter("search"))) {
                int id_ = Integer.parseInt(request.getParameter("id_eshta"));
                List<TipoEstadoReserva> lista = util.findByID(id_);

                out.println("<!DOCTYPE html>");
                out.println("<html>");

                out.println("<head>");
                out.println("<title>Registro</title>");
                 out.println("<h1>Grupo Jueves</h1>");
                out.println("<h1>GF15006</h1>");
                out.println("<h1>Entidad: tipo_estado_reserva</h1>");

                 out.println("<br/>");
                out.println("</head>");
                out.println("<body bgcolor=#9AE3CB>");

                  out.println(" <form action=\"/guia06/index.html\">");
                out.println(" <input type=\"submit\" value=\"Volver al Index\"  />");
                out.println("<br/><br/> ");
                out.println("</form> ");
                 out.println("<br/><br/> ");
                 
                out.println("<table border=1px  bgcolor=#236C01");

                out.print("<tr><td>id</td>");
                out.print("<td>Nombre</td>");
                out.print("<td>Activo</td>");
                out.print("<td>IndicaAprobacion</td>");
                out.print("<td>Observaciones</td></tr>");

                table(lista, response);
                out.print("</table>");

                out.println("<br/>");
                out.println("<br/>");

                out.println("</body>");
                out.println("</html>");
            } else if ("Buscar".equals(request.getParameter("range"))) {
                int inicio = Integer.parseInt(request.getParameter("first"));
                int fin = Integer.parseInt(request.getParameter("latest"));

                List<TipoEstadoReserva> lista = util.findRange(inicio, fin);

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet ServletCRUD</title>");
                out.println("<h1>Grupo Jueves</h1>");
                out.println("<h1>GF15006</h1>");
                out.println("<h1>Entidad: tipo_estado_reserva</h1>");
                out.println("<br/>");

                out.println("</head>");
                out.println("<body bgcolor=#9AE3CB>");

                out.println(" <form action=\"/guia06/index.html\">");
                out.println(" <input type=\"submit\" value=\"Volver al Index\"  />");
                out.println("<br/><br/> ");
                out.println("</form> ");
                
                out.println("<br/><br/> ");

                
                
                out.println("<table border=1px  bgcolor=#236C01");

                out.print("<tr><td>id</td>");
                out.print("<td>Nombre</td>");
                out.print("<td>Activo</td>");
                out.print("<td>IndicaAprobacion</td>");
                out.print("<td>Observaciones</td></tr>");

                table(lista, response);

                out.print("</table>");

                out.println("<br/>");
                out.println("<br/>");
 
                out.println("</body>");
                out.println("</html>");
            }

            if ("Crear".equals(request.getParameter("insert")) || "Actualizar".equals(request.getParameter("actualizar")) || "Mostrar".equals(request.getParameter("ver")) || "Eliminar".equals(request.getParameter("delete"))) {

                List<TipoEstadoReserva> lista = util.selectAll();

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet ServletCRUD</title>");
                out.println("<h1>Grupo Jueves</h1>");
                out.println("<h1>GF15006</h1>");
                out.println("<h1>Entidad: tipo_estado_reserva</h1>");
//                out.println(" <a href=\"/guia06/index.html\"> Volver al Index</a><br/>");
//                out.println("<br/>");

                out.println("</head>");
                out.println("<body bgcolor=#9AE3CB>");

                 out.println(" <form action=\"/guia06/index.html\">");
                out.println(" <input type=\"submit\" value=\"Volver al Index\" />");
                out.println("<br/><br/> ");
                out.println("</form> ");
                 out.println("<br/><br/> ");
                
                out.println("<table border=1px  bgcolor=#236C01");

                out.print("<tr><td>id</td>");
                out.print("<td>Nombre</td>");
                out.print("<td>Activo</td>");
                out.print("<td>IndicaAprobacion</td>");
                out.print("<td>Observaciones</td></tr>");

                table(lista, response);

                out.print("</table>");

                out.println("<br/>");
                out.println("<br/>");
                out.println("</body>");
                out.println("</html>");

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void table(List<TipoEstadoReserva> ls, HttpServletResponse response) throws IOException {
        try (PrintWriter out = response.getWriter()) {

            for (TipoEstadoReserva tipo : ls) {

                out.print("<td ><input type='text' name='id_' value='" + tipo.getIdTipoEstadoReserva() + "'/></td>");
                out.print("<td ><input type='text' name='name_' value='" + tipo.getNombre() + "'/></td>");
                out.print("<td ><input type='text' name='activo' value=' " + tipo.getActivo() + "'/> </td>");
                out.print("<td ><input type='text' name='aprobacion' value='" + tipo.getIndicaAprobacion() + "'/> </td>");
                out.print("<td ><input type='text' name='observacion' value='" + tipo.getObservaciones() + "'/> </td></tr>");
                out.print("</td></tr>");

            }
        }

    }

}
